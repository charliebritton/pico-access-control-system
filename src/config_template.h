#define UART_ID uart1
#define BAUD_RATE 9600

// TODO Figure out which UART you are using
#define UART_TX_PIN
#define UART_RX_PIN

// Onboard LED
#define LED_PIN 25

// TODO These will be implementation dependent, consult reference circuit
// diagram from SBC if using the relay module.
#define RELAY1_PIN
#define RELAY2_PIN

// TODO This can be the buzzer on the Maker Board we have, or external
#define BUZZER_PIN

// TODO Get the tag IDs from the USB console log, approve 2 of the 3 in the kit
// The last tag can be for testing that the system works correctly. 12 chars/pc
static char *allowed_tags[] = {
};

