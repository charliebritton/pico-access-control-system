#include <stdio.h>
#include <string.h>
#include "pico/stdlib.h"
#include "hardware/uart.h"
#include "config.h"

static int len = sizeof(allowed_tags)/sizeof(allowed_tags[0]);

int main() {

  // Initialise all GPIO
  // TODO Init GPIO

	// Not Reading any Sensors from GPIO, Only Writing
  // TODO Set GPIO Direction

	// Useful for USB Debugging
  stdio_init_all();

	// Setup UART Communication with RFID Module
  // TODO Setup UART Comms

  gpio_put(LED_PIN, 0);

	// Enter Loop
  while(true) {

    // RFID Tags 12 Characters in Length
    char buf[12];

    // TODO Read Tag ID into Buffer

		printf("Read data: %s\n", buf);

		// Access Granted?
		bool allowed = false;

		// Loop Through Allowed Tags
		for(size_t i = 0; i < len; ++i) {

			printf("Reading allowed tag %d, tag ID: %s, read ID: %s\n", i, allowed_tags[i], strndup(buf,12));

			if(/* TODO add condition here */) {
				allowed = true;
				break;
			}

		}

		// Actual Access Control
		if(allowed) {

				printf("Access granted. Unlocking 'door' now.\n");

				// TODO Unlock 'door', buzz and light LED

				sleep_ms(5000);

				printf("Locking 'door'.\n");

				// TODO Lock the 'door', switch LED and buzzer off

			} else {

				printf("Access denied.\n");


				// TODO Make the LED and buzzer beep a few times

		}
	}
}

