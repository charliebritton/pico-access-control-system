#include <stdio.h>
#include <string.h>
#include "pico/stdlib.h"
#include "hardware/uart.h"
#include "config.h"

static int len = sizeof(allowed_tags)/sizeof(allowed_tags[0]);

int main() {

  // Initialise all GPIO
  gpio_init(LED_PIN);
  gpio_init(BUZZER_PIN);
  gpio_init(RELAY1_PIN);
  gpio_init(RELAY2_PIN);

	// Not Reading any Sensors from GPIO, Only Writing
  gpio_set_dir(LED_PIN, GPIO_OUT);
  gpio_set_dir(BUZZER_PIN, GPIO_OUT);
  gpio_set_dir(RELAY1_PIN, GPIO_OUT);
  gpio_set_dir(RELAY2_PIN, GPIO_OUT);

	// Useful for USB Debugging
  stdio_init_all();

	// Setup UART Communication with RFID Module
  uart_init(UART_ID, BAUD_RATE);
  gpio_set_function(UART_TX_PIN, GPIO_FUNC_UART);
  gpio_set_function(UART_RX_PIN, GPIO_FUNC_UART);

  gpio_put(LED_PIN, 0);

	// Enter Loop
  while(true) {

    // RFID Tags 12 Characters in Length
    char buf[12];

		for(int i = 0; i < 12; i++) {
			buf[i] = uart_getc(UART_ID);
		}

		printf("Read data: %s\n", buf);

		// Access Granted?
		bool allowed = false;

		// Loop Through Allowed Tags
		for(size_t i = 0; i < len; ++i) {

			printf("Reading allowed tag %d, tag ID: %s, read ID: %s\n", i, allowed_tags[i], strndup(buf,12));

			if(!strcmp(allowed_tags[i], strndup(buf, 12))) {
				allowed = true;
				break;
			}

		}

		// Actual Access Control
		if(allowed) {

				printf("Access granted. Unlocking 'door' now.\n");

				gpio_put(LED_PIN, 1);
				gpio_put(BUZZER_PIN, 1);
				gpio_put(RELAY1_PIN, 1);

				sleep_ms(5000);

				printf("Locking 'door'.\n");

				gpio_put(LED_PIN, 0);
				gpio_put(BUZZER_PIN, 0);
				gpio_put(RELAY1_PIN, 0);

			} else {

				printf("Access denied.\n");

				int flag = 1;
				for(int j = 0; j < 7; j++) {

					if(flag == 0)
						flag = 1;
					else
						flag = 0;

					gpio_put(LED_PIN, flag);
					gpio_put(BUZZER_PIN, flag);
					sleep_ms(100);

			}
		}
	}
}

