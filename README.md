# Access Control System
## Repository
- [git.soton](https://git.soton.ac.uk/charlie/pico-access-control-system)
- [GitLab Mirror](https://gitlab.com/charliebritton/pico-access-control-system)

## Description
In this task you will make an access control system using an RFID module, buzzer and relay board. Components for the project can be bought at ThePiHut, at the following links:
- [RFID Board (Generic)](https://thepihut.com/products/rfid-breakout-board)
- Relay HAT [Single Channel](https://thepihut.com/products/single-channel-relay-for-raspberry-pi-pico), [Dual Channel](https://thepihut.com/products/dual-channel-relay-hat-for-raspberry-pi-pico)
This project tests understanding of GPIO, serial communication and string comparison in C.

## Tasks
Tasks for this project are outlined in the `src/access_control.c` and `src/config_template.h`. Fill in the `TODO` sections with the task outlined. A rough guide to the order is as follows:

1. Clone repo and copy `src/config_template.h` to `src/config.h`
2. Setup the `src/config.h` file with all the pin numbers, referencing the documentation to find the UART-enabled GPIO pins.
3. Focus on trying to extract the tag IDs from the UART communications.
4. Add two of the tags to the `src/config.h` so that you have both working and non-working access.
5. Check supplied tag against the config
6. Actual access control logic

## Task Classification
EASY since it's just manipulating GPIO and reading UART communications. I was able to go from idea to product in less than 2 hours so implementing this yourself shouldn't be too hard.

## Help
- [C/C++ SDK](https://raspberrypi.github.io/pico-sdk-doxygen/modules.html)
- [Python Implementation of the Reader](https://github.com/sbcshop/RFID-Breakout/blob/main/Python/rfid_read.py) (Not Specific to Pico)
- Look at the reference implementation, try and understand it, close it and then try and rewrite a bit later from memory
- Email `cb7g20@soton.ac.uk` (last resort and I can't lend hardware I'm afraid)

## License (MIT)
Copyright © 2022 Charlie Britton

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
